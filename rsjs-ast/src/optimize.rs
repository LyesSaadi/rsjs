use crate::*;

pub fn optimize(ast: &mut Option<Box<crate::Ast>>) {
    if let Some(ref mut ast) = ast {
        optimize_cmd(&mut ast.cmd);
        optimize(&mut ast.next);
    }
}

fn optimize_cmd(cmd: &mut crate::Cmd) {
    match cmd {
        Cmd::Expr(ref mut expr) => optimize_expr(expr),
        Cmd::IfElse(ref mut expr, ref mut c1, ref mut c2) => {
            optimize_expr(expr);
            optimize_cmd(c1);
            optimize_cmd(c2);
        }
        _ => {}
    }
}

fn optimize_expr(expr: &mut crate::Expr) {
    match expr {
        Expr::Assign(_, e) => optimize_expr(e),
        Expr::Neg(e) => {
            optimize_expr(e);
            if let Expr::Number(n) = **e {
                *expr = Expr::Number(-n);
            }
        }
        Expr::Not(e) => {
            optimize_expr(e);
            if let Expr::Boolean(b) = **e {
                *expr = Expr::Boolean(!b);
            }
        }
        Expr::And(lhs, rhs) => {
            optimize_expr(lhs);
            optimize_expr(rhs);
            if let (Expr::Boolean(bl), Expr::Boolean(br)) = (lhs.as_ref(), rhs.as_ref()) {
                *expr = Expr::Boolean(*bl && *br);
            }
        }
        Expr::Or(lhs, rhs) => {
            optimize_expr(lhs);
            optimize_expr(rhs);
            if let (Expr::Boolean(bl), Expr::Boolean(br)) = (lhs.as_ref(), rhs.as_ref()) {
                *expr = Expr::Boolean(*bl || *br);
            }
        }
        Expr::Op(lhs, op, rhs) => {
            optimize_expr(lhs);
            optimize_expr(rhs);
            if let (Expr::Number(nl), Expr::Number(nr)) = (lhs.as_ref(), rhs.as_ref()) {
                match *op {
                    Op::Add => *expr = Expr::Number(nl + nr),
                    Op::Sub => *expr = Expr::Number(nl - nr),
                    Op::Mul => *expr = Expr::Number(nl * nr),
                    Op::Div => *expr = Expr::Number(nl / nr),
                    Op::Eq => *expr = Expr::Boolean(nl == nr),
                    Op::NEq => *expr = Expr::Boolean(nl != nr),
                    Op::LoEq => *expr = Expr::Boolean(nl <= nr),
                    Op::GrEq => *expr = Expr::Boolean(nl >= nr),
                    Op::LoSt => *expr = Expr::Boolean(nl < nr),
                    Op::GrSt => *expr = Expr::Boolean(nl > nr),
                }
            }
        }
        _ => {}
    }
}
