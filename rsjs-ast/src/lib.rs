pub mod optimize;

pub use optimize::optimize;

pub type Ast = Program;

#[derive(Debug, PartialEq)]
pub struct Program {
    pub cmd: Box<Cmd>,
    pub next: Option<Box<Program>>,
}

#[derive(Debug, PartialEq)]
pub enum Cmd {
    Expr(Box<Expr>),
    Import(String),
    IfElse(Box<Expr>, Box<Cmd>, Box<Cmd>),
    DoWhile(Box<Cmd>, Box<Expr>),
    While(Box<Expr>, Box<Cmd>),
    Function(String, Option<Box<DArgs>>, Option<Box<Program>>),
    Return(Box<Expr>),
    Block(Option<Box<Program>>),
    Empty,
}

#[derive(Debug, PartialEq)]
pub struct DArgs {
    pub arg: String,
    pub next: Option<Box<DArgs>>,
}

#[derive(Debug, PartialEq)]
pub enum Expr {
    Identifier(String),
    Number(f64),
    Boolean(bool),
    Undefined,
    Assign(String, Box<Expr>),
    Call(String, Option<Box<Args>>),
    Op(Box<Expr>, Op, Box<Expr>),
    Neg(Box<Expr>),
    Not(Box<Expr>),
    And(Box<Expr>, Box<Expr>),
    Or(Box<Expr>, Box<Expr>),
}

#[derive(Debug, PartialEq)]
pub enum Op {
    Add,
    Sub,
    Mul,
    Div,
    LoEq,
    LoSt,
    GrEq,
    GrSt,
    Eq,
    NEq,
}

#[derive(Debug, PartialEq)]
pub struct Args {
    pub arg: Box<Expr>,
    pub next: Option<Box<Args>>,
}
