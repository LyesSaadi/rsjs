use crate::macros::{convert, jmp};
use rsjs_ast::{Args, Expr, Op};

pub(crate) fn expression(expr: Box<Expr>, out: &mut Vec<String>) {
    match *expr {
        Expr::Identifier(id) => out.push(format!("GetVar {id}")),
        Expr::Number(n) => out.push(format!("CsteNb {n}")),
        Expr::Boolean(b) => out.push(format!("CsteBo {b}")),
        Expr::Undefined => out.push(format!("CsteUn")),
        Expr::Assign(id, val) => {
            expression(val, out);
            out.push(format!("SetVar {id}"));
            out.push(format!("GetVar {id}"));
        }
        Expr::Op(l, o, r) => {
            expression(l, out);
            convert!(to_number out);
            expression(r, out);
            convert!(to_number out);
            match o {
                Op::Add => out.push(String::from("AddiNb")),
                Op::Sub => out.push(String::from("SubiNb")),
                Op::Mul => out.push(String::from("MultNb")),
                Op::Div => out.push(String::from("DiviNb")),
                Op::LoEq => out.push(String::from("LoEqNb")),
                Op::LoSt => out.push(String::from("LoStNb")),
                Op::GrEq => out.push(String::from("GrEqNb")),
                Op::GrSt => out.push(String::from("GrStNb")),
                Op::Eq => out.push(String::from("Equals")),
                Op::NEq => out.push(String::from("NotEql")),
            };
        }
        Expr::Neg(e) => {
            expression(e, out);
            convert!(to_number out);
            out.push(String::from("NegaNb"))
        }
        Expr::Not(e) => {
            expression(e, out);
            convert!(to_bool out);
            out.push(String::from("Not"))
        }
        Expr::And(lhs, rhs) => {
            expression(lhs, out);
            convert!(to_bool out);
            jmp!(out, "ConJmp", {
                expression(rhs, out);
                convert!(to_bool out);
                out.push(String::from("Jump 1"));
            });
            out.push(String::from("CstBo false"));
        }
        Expr::Or(lhs, rhs) => {
            expression(lhs, out);
            convert!(to_bool out);
            out.push(String::from("Not"));
            jmp!(out, "ConJmp", {
                expression(rhs, out);
                convert!(to_bool out);
                out.push(String::from("Jump 1"));
            });
            out.push(String::from("CstBo true"));
        }
        Expr::Call(id, args) => {
            out.push(format!("GetVar {}", id));
            out.push(String::from("StCall"));
            arguments(args, out);
            out.push(String::from("Call"));
        }
    }
}

fn arguments(args: Option<Box<Args>>, out: &mut Vec<String>) {
    if let Some(args) = args {
        arguments(args.next, out);
        expression(args.arg, out);
        out.push(String::from("SetArg"));
    }
}
