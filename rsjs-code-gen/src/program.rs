use crate::{command::command, error::Result};
use rsjs_ast::Program;

pub fn program(prg: Option<Box<Program>>, out: &mut Vec<String>) -> Result<()> {
    if let Some(prg) = prg {
        command(prg.cmd, out)?;
        program(prg.next, out)?;
    }
    Ok(())
}
