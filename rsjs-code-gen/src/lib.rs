pub(crate) mod command;
pub mod error;
pub(crate) mod expression;
pub(crate) mod macros;
pub(crate) mod program;

use program::program;
use rsjs_ast::Program;

pub fn code_gen(prg: Option<Box<Program>>, out: &mut Vec<String>) -> error::Result<()> {
    program(prg, out)?;
    out.push(String::from("Halt"));
    Ok(())
}
