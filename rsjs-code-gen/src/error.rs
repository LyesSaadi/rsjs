use thiserror::Error;

pub type Result<T> = std::result::Result<T, CodeGenError>;

#[derive(Error, Debug)]
pub enum CodeGenError {
    #[error("unable to import : {0}")]
    ImportError(#[from] std::io::Error),
}
