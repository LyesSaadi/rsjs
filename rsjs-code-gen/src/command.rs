use std::{fs::File, io::Read};

use crate::{error, expression::expression, macros::*, program::program};
use rsjs_ast::{Cmd, DArgs};

pub(crate) fn command(cmd: Box<Cmd>, out: &mut Vec<String>) -> error::Result<()> {
    match *cmd {
        Cmd::Expr(expr) => {
            expression(expr, out);
            out.push("Drop".to_owned());
        }
        Cmd::Import(ident) => {
            let mut file = File::open(format!("{ident}.jsm"))?;
            let mut code = String::new();
            file.read_to_string(&mut code)?;
            for l in code.split('\n') {
                out.push(l.to_owned());
            }
        }
        Cmd::IfElse(e, c1, c2) => {
            expression(e, out);
            convert!(to_bool out);
            jmp!(
                out,
                "ConJmp",
                {
                    command(c1, out)?;
                },
                1
            );
            jmp!(out, "Jump", {
                command(c2, out)?;
            });
        }
        Cmd::DoWhile(c, e) => {
            let n = count!(out, {
                command(c, out)?;
                expression(e, out);
                convert!(to_bool out);
                out.push(String::from("Not"));
            });
            out.push(format!("ConJmp {}", -n - 2));
        }
        Cmd::While(e, c) => {
            let n = count!(out, {
                expression(e, out);
                convert!(to_bool out);
                jmp!(
                    out,
                    "ConJmp",
                    {
                        command(c, out)?;
                    },
                    1
                );
            });
            out.push(format!("Jump {}", -n - 2));
        }
        Cmd::Function(name, args, body) => {
            jmp!(
                out,
                "NewClot",
                {
                    decl_args(args, out);
                    out.push(format!("SetVar {}", name));
                },
                1
            );
            jmp!(out, "Jump", {
                program(body, out)?;
            });
        }
        Cmd::Return(e) => {
            expression(e, out);
            out.push(String::from("Return"));
        }
        Cmd::Block(p) => {
            program(p, out)?;
        }
        Cmd::Empty => {}
    }
    Ok(())
}

fn decl_args(args: Option<Box<DArgs>>, out: &mut Vec<String>) {
    if let Some(args) = args {
        out.push(format!("DeclArg {}", args.arg));
        decl_args(args.next, out);
    }
}
