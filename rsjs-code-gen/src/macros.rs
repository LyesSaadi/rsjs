macro_rules! count {
    ($out:ident, $exc:block) => {{
        let n = $out.len();
        $exc;
        let mut skip = 0;
        if $out.len() > n {
            for l in &mut $out[n + 1..] {
                let l = &l.trim();
                if l.len() > 0 && !l.starts_with("#") {
                    skip += 1;
                }
            }
        }
        skip
    }};
}

pub(crate) use count;

macro_rules! jmp {
    ($out:ident, $inst:expr, $exc:block, $offset:expr) => {{
        let n = $out.len();
        $out.push(String::from("TO CHANGE"));

        let skip = crate::macros::count!($out, $exc);

        $out[n] = format!("{} {}", $inst, skip + 1 + $offset);
        skip
    }};
    ($out:ident, $inst:expr, $exc:block) => {
        crate::macros::jmp!($out, $inst, $exc, 0)
    };
}

pub(crate) use jmp;

macro_rules! case {
    (
        $out:ident,
        $($n:literal => { $($str:expr),* }),*
    ) => {
        let mut table: Vec<Vec<String>> = Vec::new();
        let mut max: usize = 0;

        $(
            let lines = vec![$($str),*];
            if max < lines.len() { max = lines.len() }
            while table.len() <= $n {
                table.push(Vec::new());
            }
            table[$n] = lines;
        )*

        let mut i = 1;
        let l = table.len();
        for t in &mut table {
            while t.len() < max {
                t.push(String::from("Noop"));
            }
            t.push(format!("Jump {}", (l - i) * (max + 1)));
            i = i + 1;
        }

        let mut table = table.iter().flatten().map(|o| o.to_owned()).collect();

        $out.push(format!("Case {}", max + 1));

        $out.append(&mut table);
    };
}

pub(crate) use case;

macro_rules! convert {
    (to_bool $out:ident) => {
        $out.push(String::from("TypeOf"));
        crate::macros::case!($out,
            1 => { String::from("NbToBo") },
            2 => { String::from("Error") }, // Strings are not implemented
            3 => { String::from("Drop"), String::from("CstBo false") },
            4 => { String::from("Error") }, // clotures shouldn't be casted
            5 => { String::from("Error") } // neither should objects
        );
    };
    (to_number $out:ident) => {
        $out.push(String::from("TypeOf"));
        crate::macros::case!($out,
            0 => { String::from("BoToNb") },
            2 => { String::from("Error") },
            3 => { String::from("Drop"), String::from("CstNb NaN") },
            4 => { String::from("Error") },
            5 => { String::from("Error") }
        );
    };
}

pub(crate) use convert;
