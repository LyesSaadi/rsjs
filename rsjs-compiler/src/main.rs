use clap::Parser;
use std::{
    fs::File,
    io::{self, Read, Write},
    path::PathBuf,
};

use rsjs_ast;
use rsjs_code_gen;
use rsjs_parser;

/// A Rusty JavaScript Compiler
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File to take the JS code from, if omitted, enters into evaluation mode
    // #[arg(short, long)]
    input: Option<PathBuf>,

    /// Display the generated AST
    #[arg(long, default_value_t = false)]
    ast: bool,

    /// File to output the Assembly code to, if omitted, prints to stdout
    #[arg(short, long)]
    output: Option<PathBuf>,
}

fn main() {
    let args = Args::parse();

    let mut output = if let Some(o) = args.output {
        Some(File::create(o).expect("Unable to create file"))
    } else {
        None
    };

    if let Some(f) = args.input {
        let mut file = File::open(f).expect("Unable to open input file");
        let mut buf = String::new();
        file.read_to_string(&mut buf).unwrap();
        compile(&buf, args.ast, &mut output);
    } else {
        let mut buf = String::new();

        println!("> Entering eval mode, Ctrl-C to quit, --help to see options");

        loop {
            print!("> ");
            io::stdout().flush().unwrap();

            if std::io::stdin().read_line(&mut buf).is_err() {
                break;
            }

            compile(&buf, args.ast, &mut output);

            buf = String::new();

            println!("");
        }
    };
}

fn compile(input: &String, ast: bool, output: &mut Option<File>) {
    let result = rsjs_parser::parse(input);

    if result.is_err() || ast {
        println!("{:#?}", result);
    }

    if let Ok(mut result) = result {
        rsjs_ast::optimize(&mut result);
        let mut code = Vec::new();
        if let Err(e) = rsjs_code_gen::code_gen(result, &mut code) {
            println!("{e}");
        } else {
            if let Some(o) = output {
                o.write(code.join("\n").as_bytes())
                    .expect("Unable to write to output");
            } else {
                println!("{}", code.join("\n"));
            }
        }
    }
}
