pub mod error;
pub mod lexer;
pub mod parser;

pub use lexer::Lexer;
pub use parser::Parser;

pub fn parse<T: AsRef<str>>(input: T) -> error::Result<Option<Box<rsjs_ast::Ast>>> {
    let lexer = Lexer::new(input.as_ref());
    let parser = Parser::new();
    Ok(parser.parse(lexer)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rsjs_ast::*;

    #[test]
    fn prio_number() {
        let code = "3 + (4.5 * 2) / 2;";
        assert_eq!(
            parse(code).unwrap().unwrap().cmd,
            Box::new(Cmd::Expr(Box::new(Expr::Op(
                Box::new(Expr::Number(3.)),
                Op::Add,
                Box::new(Expr::Op(
                    Box::new(Expr::Op(
                        Box::new(Expr::Number(4.5)),
                        Op::Mul,
                        Box::new(Expr::Number(2.))
                    )),
                    Op::Div,
                    Box::new(Expr::Number(2.))
                ))
            ))))
        );
    }

    #[test]
    fn prio_boolean() {
        let code = "(5 <= 4) == True != (5 > 5);";
        assert_eq!(
            parse(code).unwrap().unwrap().cmd,
            Box::new(Cmd::Expr(Box::new(Expr::Op(
                Box::new(Expr::Op(
                    Box::new(Expr::Op(
                        Box::new(Expr::Number(5.)),
                        Op::LoEq,
                        Box::new(Expr::Number(4.))
                    )),
                    Op::Eq,
                    Box::new(Expr::Boolean(true))
                )),
                Op::NEq,
                Box::new(Expr::Op(
                    Box::new(Expr::Number(5.)),
                    Op::GrSt,
                    Box::new(Expr::Number(5.))
                ))
            ))))
        );
    }
}
