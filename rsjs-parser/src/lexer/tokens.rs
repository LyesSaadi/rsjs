use logos::{Lexer, Logos};
use std::fmt::{Display, Formatter, Result};

#[derive(Logos, Clone, Debug, PartialEq)]
#[logos(skip r"[ \t\n\f]+|//[^\n]*\n*|/\*[^*]*\*+([^/][^*]*\*+)*/")]
pub enum Token {
    // Language separators
    #[token(";")]
    Semicolon,
    #[token(",")]
    Comma,

    #[token("(")]
    LParen,
    #[token(")")]
    RParen,
    #[token("{")]
    LBrack,
    #[token("}")]
    RBrack,

    // Types
    #[regex(r"[a-zA-Z_][a-zA-Z0-9_]*", |lex| lex.slice().to_owned())]
    Identifier(String),

    #[regex(
        r"(0\.?|[1-9][0-9]*\.?|\.?[0-9])[0-9]*(e\-?[0-9]+)?|NaN",
        number,
        priority = 2
    )]
    Number(f64),

    #[regex(r"true|false", |lex| lex.slice().contains("true"))]
    Boolean(bool),

    #[regex("undefined")]
    Undefined,

    // Keywords
    #[token("import")]
    Import,
    #[token("if")]
    If,
    #[token("else")]
    Else,
    #[token("do")]
    Do,
    #[token("while")]
    While,
    #[token("function")]
    Function,
    #[token("return")]
    Return,

    // Operators
    #[token("=")]
    Assign,

    #[token("+")]
    OpAdd,
    #[token("-")]
    OpSub,
    #[token("*")]
    OpMul,
    #[token("/")]
    OpDiv,

    #[token("&&")]
    OpAnd,
    #[token("||")]
    OpOr,
    #[token("==")]
    OpEq,
    #[token("!=")]
    OpNotEq,
    #[token("!")]
    OpNot,
    #[token("<=")]
    OpLowEq,
    #[token("<")]
    OpLowSt,
    #[token(">=")]
    OpGreEq,
    #[token(">")]
    OpGreSt,
}

impl Display for Token {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{:?}", self)
    }
}

fn number(lex: &mut Lexer<Token>) -> Option<f64> {
    if lex.slice().contains("NaN") {
        return Some(f64::NAN);
    }
    lex.slice().parse().ok()
}
