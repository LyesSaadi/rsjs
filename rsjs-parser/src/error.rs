use thiserror::Error;

pub type Result<T> = std::result::Result<T, ParseError>;

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("unable to read the input : {0}")]
    IO(#[from] std::io::Error),
    #[error("unable to parse the code : {0:#?}")]
    Parser(
        #[from]
        lalrpop_util::ParseError<
            usize,
            crate::lexer::tokens::Token,
            crate::lexer::lexer::LexicalError,
        >,
    ),
}
