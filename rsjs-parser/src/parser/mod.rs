use lalrpop_util::lalrpop_mod;

lalrpop_mod!(pub parser, "/parser/grammar.rs");

pub use parser::ProgramParser as Parser;
