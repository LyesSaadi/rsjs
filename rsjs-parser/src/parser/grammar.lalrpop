use crate::lexer::{
	tokens::Token,
	lexer::LexicalError,
};

use rsjs_ast::*;

grammar;

pub Program: Option<Box<Program>> = {
	<cmd:Command> <next:Program> => Some(Box::new(Program { cmd, next })),
	() => None,
}

pub Command: Box<Cmd> = {
	<expr:Expression> ";" => Box::new(Cmd::Expr(expr)),
	"import" <id:"ident"> ";" => Box::new(Cmd::Import(id)),
	"if" "(" <expr:Expression> ")" <c1:Command> "else" <c2:Command> => Box::new(Cmd::IfElse(expr, c1, c2)),
	"do" <c:Command> "while" "(" <e:Expression> ")" ";" => Box::new(Cmd::DoWhile(c, e)),
	"while" "(" <e:Expression> ")" <c:Command> => Box::new(Cmd::While(e, c)),
	"function" <id:"ident"> "(" <args:DeclArgs> ")" "{" <body:Program> "}" => Box::new(Cmd::Function(id, args, body)),
	"return" <e:Expression> ";" => Box::new(Cmd::Return(e)),
	"{" <p:Program> "}" => Box::new(Cmd::Block(p)),
	";" => Box::new(Cmd::Empty),
}

pub DeclArgs: Option<Box<DArgs>> = {
	<arg:"ident"> "," <next:DeclArgs> => Some(Box::new(DArgs { arg, next })),
	<arg:"ident"> => Some(Box::new(DArgs { arg, next: None })),
	() => None,
}

ExprParen: Box<Expr> = "(" <Expression> ")";

Expression: Box<Expr> = {
	#[precedence(level="0")]
	<"ident"> => Box::new(Expr::Identifier(<>)),
	<"number"> => Box::new(Expr::Number(<>)),
	<"bool"> => Box::new(Expr::Boolean(<>)),
	<"undefined"> => Box::new(Expr::Undefined),
	"-" <Expression> => Box::new(Expr::Neg(<>)),
	"!" <Expression> => Box::new(Expr::Not(<>)),
	<f:"ident"> "(" <args:Arguments> ")" => Box::new(Expr::Call(f, args)),
	ExprParen,
	#[precedence(level="1")] #[assoc(side="left")]
	<lhs:Expression> "*" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::Mul, rhs)),
	<lhs:Expression> "/" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::Div, rhs)),
	#[precedence(level="2")] #[assoc(side="left")]
	<lhs:Expression> "+" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::Add, rhs)),
	<lhs:Expression> "-" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::Sub, rhs)),
	#[precedence(level="3")] #[assoc(side="left")]
	<lhs:Expression> "<=" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::LoEq, rhs)),
	<lhs:Expression> "<" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::LoSt, rhs)),
	<lhs:Expression> ">=" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::GrEq, rhs)),
	<lhs:Expression> ">" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::GrSt, rhs)),
	#[precedence(level="4")] #[assoc(side="left")]
	<lhs:Expression> "==" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::Eq, rhs)),
	<lhs:Expression> "!=" <rhs:Expression> => Box::new(Expr::Op(lhs, Op::NEq, rhs)),
	#[precedence(level="5")] #[assoc(side="left")]
	<lhs:Expression> "&&" <rhs:Expression> => Box::new(Expr::And(lhs, rhs)),
	<lhs:Expression> "||" <rhs:Expression> => Box::new(Expr::Or(lhs, rhs)),
	#[precedence(level="6")]
	<id:"ident"> "=" <rhs:Expression> => Box::new(Expr::Assign(id, rhs)),
}

Arguments: Option<Box<Args>> = {
	<arg:Expression> "," <next:Arguments> => Some(Box::new(Args { arg, next })),
	<arg:Expression> => Some(Box::new(Args { arg, next: None })),
	() => None,
}

extern {
	type Location = usize;
	type Error = LexicalError;

	enum Token {
		";" => Token::Semicolon,
		"," => Token::Comma,

		"(" => Token::LParen,
		")" => Token::RParen,
		"{" => Token::LBrack,
		"}" => Token::RBrack,

		"ident" => Token::Identifier(<String>),
		"number" => Token::Number(<f64>),
		"bool" => Token::Boolean(<bool>),
		"undefined" => Token::Undefined,

		"import" => Token::Import,
		"if" => Token::If,
		"else" => Token::Else,
		"do" => Token::Do,
		"while" => Token::While,
		"function" => Token::Function,
		"return" => Token::Return,

		"=" => Token::Assign,

		"+" => Token::OpAdd,
		"-" => Token::OpSub,
		"*" => Token::OpMul,
		"/" => Token::OpDiv,

		"&&" => Token::OpAnd,
		"||" => Token::OpOr,
		"==" => Token::OpEq,
		"!=" => Token::OpNotEq,
		"!" => Token::OpNot,
		"<=" => Token::OpLowEq,
		"<" => Token::OpLowSt,
		">=" => Token::OpGreEq,
		">" => Token::OpGreSt,
	}
}
