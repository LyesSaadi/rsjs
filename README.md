# rsjs - A Rusty JavaScript Compilator

## What is this ?
This is a project to write a JavaScript compiler as part of the end of the year's project for the Compilation course of the 3rd year of Bachelor in Computer Science at the USPN (Paris-XIII).
This project should've been written in C, OCaml or Java, but, wanting to participate in the effort for Rust World Domination, I decided to do it in Rust :D !

## How to run this ?

### Installing Rust
You first need to install the rust compiler `rustc`, and the rust package manager `cargo`. For that, it is recommended to use `rustup`. For that, follow the instruction [on the website](https://rustup.rs/).

For Linux, you simply need to run:
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Running the Project
You simply need to run on the root of the project, or in `rsjs-compiler/`, this command:
```bash
cargo run
```

This will open by default an "interpreter" that will output the "assembly" that you need to run on the [Mini-JS Machine](https://lipn.univ-paris13.fr/~breuvart/compilation/interpreteurJS/).

You can find more options by displaying the help page by running:
```bash
# Notice the -- before the help option. Otherwise, cargo picks it up.
cargo run -- --help
```

## Structure
The rsjs workspace is separated as follow :
- `rsjs-compiler` : Contains the rsjs compiler, it is the entry point for executing the project
- `rsjs-parser` : Contains the rsjs parser and lexer, written using LALRPOP
- `rsjs-ast` : Contains the rsjs AST structure, with its optimization algorithms
- `rsjs-code-gen` : Contains the rsjs code generation code
