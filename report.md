---
title: Projet de Compilation
subtitle: L3 Double Licence
author: Lyes Saadi (12107246)
date: "26 Mai 2024"
header-includes: |
    \usepackage{fancyhdr}
    \usepackage{lastpage}
    \pagestyle{fancy}
    \fancyfoot[CO,CE]{}
    \fancyfoot[LO,RE]{Lyes Saadi (12107246)}
    \fancyfoot[LE,RO]{\thepage/\pageref{LastPage}}
geometry: "right=2cm,left=2cm,top=4cm,bottom=4cm"
output: pdf_document
...

# Présentation

## Projet

RSJS est un compilateur de JavaScript vers le
langage assembleur de la mini-JS Machine. Il est hébergé sur
[gitlab.com/LyesSaadi/rsjs](https://gitlab.com/LyesSaadi/rsjs) (lien cliquable).
Il est réalisé en Rust avec les librairies
[LALRPOP](https://crates.io/crates/lalrpop) (qui n'utilise pas LALR
contrairement à ce qu'indique son nom) et
[Logos](https://crates.io/crates/logos) (Lexeur). Pour compiler le projet, seul
le compilateur Rust est nécessaire.

## Membres du groupe
- Lyes Saadi

### Répartition des responsabilités
- **Team Leader :** Lyes Saadi
- **Développeur principal :** Lyes Saadi
- **Développeur du parseur :** Lyes Saadi
- **Développeur de l'AST :** Lyes Saadi
- **Développeur de la génération de code :** Lyes Saadi
- **Testeur :** Lyes Saadi
- **Documentation :** Lyes Saadi
- **Graphismes :** Lyes Saadi
- **Ingénieur UX :** Lyes Saadi
- **Forgeron :** Lyes Saadi
- **Approbation pour cette blague :** Lydia Bakiri

## Branches

- `parser_work` : branche de travail sur le parseur
- `parser` : contient le parseur
- `interpreter` : contient l'interpréteur primitif
- `parser_test` : contient une suite primitive de test pour le parseur (s'exécute avec `cargo test`)
- `ast` : contient l'AST et les optimisations par analyse sémentique
- `code_gen` : contient la génération de code
- `main` : contient le compilateur final

## Versionnage

Les tags commençant par `p` sont des tags sur le fragment implémenté pour le
parseur, les tags commençant par `i` sont des tags sur le fragment implémenté
pour l'interpréteur, les tags commençant par `c` sont des tags sur le fragment
implémenté pour la génération de code. Les tags sans préfixe sont les versions
du logiciel dans main.

# Fragments implémentés

Les gros fragments 0, 1, 2, 3, 4 et 5 sont implémentés, sauf le dernier fragment
optionnel du 5. Les fragments étaient quelques fois mal numérotés, j'ai essayé
de faire de mon mieux pour suivre ça. Certains fragments ont été fait dans le
désordre. Pour d'autres, il n'ont pas demandé autant d'implémentation que
demandé, soit parce que le problème ne se pose pas en Rust, soit parce que
j'avais anticipé les fragments.

## Erreurs sur l'exécution de certains fragments

Quelques fois, des fragments ont été mal implémenté. Dans ces cas, un tag avec
un sous-versionnage (exemple p4.1.1) a été émis pour rectifier l'erreur. J'ai
aussi commencé à faire partiellement l'implémentation du 4.1 en avance parce que
le sujet avait certains mot clés en minuscule, ce qui m'avait confus, et j'ai
mélangé mots clés avec majuscule et minuscule à certains moments du projet. Je
m'excuse d'avance pour cela.

# Libertés prises

## Structure de l'AST

J'ai décidé d'implémenter l'AST avec des énumérations sur le tas. Les
énumérations étant plus puissantes en Rust et ayant ce pouvoir expressif
supplémentaire avec le pattern matching, j'ai décidé de les utiliser
directement. Ces énumérations étant récursives, il est nécessaire de les
stocker sur le tas, leur taille n'étant pas connu à la compilation.

## Gestion du nombre de lignes

J'ai décidé de gérer le nombre de ligne non pas en stockant l'information dans
mon AST, mais en utilisant des macros en Rust et en comptant le nombre de ligne
généré par la génération de code directement. Ma génération de code utilisant un
vecteur de chaînes de caractères, cela est relativement aisé à implémenter.
Et, en utilisant une suite de macros, cela permet de rendre ça plutôt
ergonomique et évite les bugs suite à des changements de nombre d'instruction
par exemple. Cela me permet surtout d'implémenter les imports sans gros
changement. Et ça simplifie plein d'autres d'aspects comme les cases.

Voici le détail des macros :
```rust
// la macro de base, il renvoie le nombre de ligne généré par $exc
count!($out, $exc)

// fait un jump ou un cndjmp (selon la valeur d'$inst)
// $offset permet de d'augmenter le jump
jmp!($out, $inst, $exc, $offset = 0)

// permet de faire des case avec une structure assez ergonomique, il a besoin du
// code en avance pour pouvoir gérer les Noop, d'où le passage des lignes
// directement dans le case à la place de l'exécution du code comme cela est
// habituellement fait
case!($out, ($n => { $str* })*)

// permet de générer le code pour convertir une valeur en bool ou number
convert!(to_bool|to_number $out)
```

Néanmoins, j'ai un petit bug sur mon implémentation actuelle où les jumps sont
mal calculé lors qu'on done un block vide dans plusieurs structures. Je pense
avoir identifié la ligne coupable, mais je n'ai pas le temps d'implémenter ces
changements actuellement et de tout retester. Je règlerai ça mes ennuis présents
passés (et après la fin de votre correction des projets, sinon, ça serait de la
triche).

## Propreté de la pile et verbosité de la génération de code

J'ai fait en sorte de garder la pile aussi propre que possible dès mon
implémentation initiale. Néanmoins, cela veut aussi dire que ma génération
de code est assez verbeuse (y a des Drop partout et étapes inutiles). C'est une
bonne piste d'amélioration du code que d'éviter ces étapes inutiles.

J'ai aussi décidé, à cause de manque de temps pour finir le fragment 5 que
j'avais entamé, de tout convertir même si le type initial est le bon. C'est à
améliorer (c'est relativement trivial à faire), mais, en attendant, ça marche
bien.

# Note sur les librairies utilisées

## LALRPOP

LALRPOP est bizarre dans quelques un de ses choix (comme l'histoire
d'indirection avec les parenthèses). Je reste néanmoins d'opinion que ça a été
le bon choix à faire. Il a été assez agréable de travailler avec cette librairie.

## Logos

L'utilisation d'un lexeur séparé a fait que j'ai dû être plus verbeux en
définissant deux fois chaque tokens "simple" (dans `token.rs` et
`grammar.lalrpop`), mais c'est nécessaire pour séparer les deux.
Sinon, il n'y a sa eu à gérer de problème de priorité dans le fragment 4.1
grâce à Logos, ce dernier ayant un algorithme qui gère ça automatiquement. Le
seul problème de priorité qu'il y a eu était avec NaN, qui fut géré en ajoutant
`priority=2`.

# Conclusion

Ce fut une très bonne expérience que d'utiliser Rust pour ce projet. Néanmoins,
je suis assez triste de ne pas avoir pu le finir à cause de mes histoires
d'orientation. Je regrette aussi le manque de documentation et de de tests que
j'ai dû sauter à cause du manque de temps. En tout cas, je me ferai une joie de
revenir sur ce projet dès que tout cela sera finit.

Et j'espère que vous saurez excuser mon petit trait d'humour à la page 1 ;).
